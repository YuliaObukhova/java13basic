package secondweekpractice;

import java.util.Scanner;

public class Task4 {

    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 110;
    public static final int STANDARD_PRICE = 100;

    public static void main(String[] args) {

    Scanner input = new Scanner(System.in);
    int roomType = input.nextInt();

        switch (roomType) {
            case 1 -> System.out.println("VIP Price: " + VIP_PRICE);
            case 2 -> System.out.println("Premium Price: " + PREMIUM_PRICE);
            case 3 -> System.out.println("Standard Price: " + STANDARD_PRICE);
            default -> System.out.println("Введите корректный номер");
        }
    }
}
