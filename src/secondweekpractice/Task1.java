package secondweekpractice;

import java.util.Scanner;

/*
Дано число n. Нужно проверить четное ли оно.
*/

public class Task1 {
    public static void main(String[] args) {
        int a;

        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        String str;

        str = (a % 2 == 0) ? " число нечётное" : " число нечётное";
           System.out.println(a + str);

//        if (a % 2 == 0) {
//            str = " число чётное";
//        }
//        else {
//            str = " число нечётное";
//        }
//        System.out.println(a + str);

//        if(a % 2 == 0) {
//            System.out.print("Чётное");
//        }
//        else {
//            System.out.print("Нечётное");
//        }
    }
}
