package secondweekpractice;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        String pattern = input.next();

        String upperPattern = pattern.toUpperCase();
        System.out.println(str.replace(pattern, upperPattern));
    }
}
