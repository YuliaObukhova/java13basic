package thirdweekpractice.regexp;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        String i = new Scanner(System.in).nextLine();
        System.out.println("Initial value: " + i);
        System.out.println("Updated value: " + i.replaceAll("([a-z])([A-Z]+)",
                                                        "$1_$2").toLowerCase());
    }
}
