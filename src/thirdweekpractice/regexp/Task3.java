package thirdweekpractice.regexp;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner i = new Scanner(System.in);
        String name = i.nextLine();
        String date = i.nextLine();
        String phone = i.nextLine();
        String email = i.nextLine();

        name.matches("[A-Z][a-z]{1,19}");
        date.matches("\\d{2}\\.\\d{2}\\.\\d{4}");
        phone.matches("\\+[0-9]{11}");
        email.matches("[a-zA-Z0-9\\_\\-\\*\\.]+@[a-z0-9]+\\.(com|ru)");

    }
}
