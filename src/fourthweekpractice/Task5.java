package fourthweekpractice;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int b = scanner.nextInt();

        int sum = 0;
        while (b > 0) {

            sum = sum + b % 10;
            System.out.println("SUM in WHILE LOOP: " + sum);
            b = b / 10;
            System.out.println("N in WHILE LOOP: " + b);

        }
        System.out.println("Answer: " + sum);
    }
}