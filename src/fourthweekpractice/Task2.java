package fourthweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int res = 1;
        for (int i = n; i <= n; i++) {
            res = res * i;
        }
            System.out.println(res);
    }
}
