package fourthweekpractice;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int res = 1;
        for (int i = 2; i <= n; i++) {
            res *= i;
            System.out.println("промежуточный результат: " + res);

        }
        System.out.println("Итоговый результат: " + res);
    }
}
