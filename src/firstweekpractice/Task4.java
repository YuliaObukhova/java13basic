package firstweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
          double s = input.nextDouble();

          double diameter = Math.sqrt(s * 4 / Math.PI);
          double circLength = Math.PI * diameter;

        System.out.println("Диаметр окружности: " + diameter);
        System.out.println("Длина окружности: " + circLength);

    }
}
